#include <stdio.h>
#include <stdlib.h>

#include "factorial.h"

int main(){
  int numero;
  int resultado;

  printf("Dime un numero");
  scanf("%i", &numero);

  resultado = factorial(numero);

  printf("El factorial de %i es %i \n", numero, resultado);

  return EXIT_SUCCESS;
}
