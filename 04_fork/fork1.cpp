#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>


int main (){

	pid_t child_pid;

	printf("El ID del proceso padre es %i\n", (int) getpid());

	child_pid = fork();

	if(child_pid !=0){
		printf("Este es el ID del proceso padre : %i\n", (int) getpid());
		printf("El ID del proceso hijo : %i\n", (int) child_pid);
	}else
		printf("Este es el ID del proceso hijo con el ID : %i\n", (int) getpid());


	return EXIT_SUCCESS;

}
