#ifndef __LIB2_H__
#define __LIB2_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"{
#endif
int multiplicar(int op1, int op2);
int factorial(int op1);

#ifdef __cplusplus
}

#endif
#endif
