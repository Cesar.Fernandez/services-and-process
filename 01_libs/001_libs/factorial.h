#ifndef __FACTORIAL_H__
#define __FACTORIAL_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

int factorial(int n);

#ifdef __cplusplus
}

#endif

#endif
