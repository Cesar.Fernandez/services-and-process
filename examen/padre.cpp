#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#define HIJO "./hijo"
#define SIGNALS 3

int spawn (char* program, char** arg_list){
	pid_t child_pid;
	child_pid = fork();

	if(child_pid!=0){
		return child_pid;
	}else{
		execvp(program, arg_list);
		fprintf(stderr, "Ha ocurriod un error\n");
		abort();
	}
}


int main (){
	pid_t child_pid;
	int child_status;
	char* arg_list[] = {(char *) HIJO, NULL};

	child_pid = spawn((char *) HIJO, arg_list);

	int p = 0;

	while(p < SIGNALS){
		sleep(1);
		p++;
		kill(child_pid, SIGUSR1);
		printf("Señales enviadas : %i\n", p);
	}

	wait(&child_status);

	if(WIFEXITED(child_status))
		printf("El hijo se ha ejecutado correctamente\n");
	else
		printf("Error al ejecutar el hijo\n");


	return EXIT_SUCCESS;

}
