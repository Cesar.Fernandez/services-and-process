#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int counter;

void* myFunc(void *arg){


	pthread_mutex_lock(&mutex);

	counter += 1;

	printf("Job %i started \n", counter);

	sleep(1);

	printf("Job %i finished \n", counter);
	sleep(1);

	pthread_mutex_unlock(&mutex);

	return NULL;
}

int main (){

	int i= 0;

	pthread_t thread[2];


	while(i<2){
		pthread_create(&(thread[i]), NULL, myFunc, NULL);

		i++;
	}

	pthread_join(thread[0], NULL);
	pthread_join(thread[1], NULL);

	pthread_mutex_destroy(&mutex);




	return EXIT_SUCCESS;

}
