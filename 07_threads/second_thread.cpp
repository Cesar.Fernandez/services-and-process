#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


void* myTurn(void * arg){
  int *iptr = (int *) arg;
  for(int i=0; i<8;i++){
    sleep(1);
    printf("Is my Turn! %i , %i\n", i, *iptr);
    (*iptr)++;
  }

    return NULL;
}

void yourTurn() {
    for(int i=0; i<4; i++){
      sleep(2);
      printf("Now is your turn! %i\n", i);
    }
}

int main(){

    pthread_t newthread;
    int v = 5;

    pthread_create(&newthread, NULL, myTurn, &v);

    yourTurn();

    pthread_join(newthread, NULL);

    printf("Thread´s done %i\n", v);


    return EXIT_SUCCESS;
}


