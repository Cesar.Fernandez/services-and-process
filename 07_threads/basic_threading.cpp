#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *myFunc(void * value){
    int *num = (int*) value;

    pthread_mutex_lock(&mutex);
    printf("Hello from the second thread\n");
    
    sleep(1);
    printf("The value of value is %i\n", *num);

    pthread_mutex_unlock(&mutex);


  return NULL;
}

void imprime(){
    sleep(1);
  printf("Helo from the main thread\n");
}

int main (){
    pthread_t thread;
    int num = 123;

    pthread_create(&thread, NULL, myFunc, &num);

    imprime();

    pthread_join(thread, NULL);

    pthread_mutex_destroy(&mutex);


	return EXIT_SUCCESS;

}
