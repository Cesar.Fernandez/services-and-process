#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>


pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t count;

void* thread(void* arg){

	sem_wait(&count);

	printf("\nEntered...\n");
	
	pthread_mutex_lock(&mutex);
	//Critical section
	sleep(3);

	pthread_mutex_unlock(&mutex);

	printf("\nJust Exiting...\n");

	sem_post(&count);

	return NULL;
}




int main (){

	pthread_t t1, t2;
	sem_init(&count, 0, 1);

	pthread_create(&t1, NULL, thread, NULL);
	sleep(2);
	pthread_create(&t2, NULL, thread, NULL);
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);

	sem_destroy(&count);
	pthread_mutex_destroy(&mutex);


	return EXIT_SUCCESS;

}
