#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void* myTurn(void * arg){
    int *iptr = (int *)malloc(sizeof(int));
    *iptr = 5;
    for(int i=0; i<8; i++){
      sleep(1);
      printf("My Turn! %i | %i\n", i , *iptr);
      (*iptr)++;
    }
  return iptr;
}

void yourTurn(){
    for(int i=0; i<3;i++){
      sleep(2);
      printf("Your Turn %i\n", i);
    }
}

int main (){

    pthread_t newthread;
    int *result;


    pthread_create(&newthread, NULL, myTurn, NULL);
    
    yourTurn();

    pthread_join(newthread, (void**)&result);

    printf("Thread done. *result=%i\n", *result);


	return EXIT_SUCCESS;

}
