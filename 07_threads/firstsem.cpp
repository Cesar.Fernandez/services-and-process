#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <time.h>
#include <signal.h>

#define NCAJAS 7
#define SIZEROW 50

sem_t semaphore;
sig_atomic_t cajas_ocupadas = 0;
sig_atomic_t fin = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void init(int cajas_totales){
	sem_init(&semaphore, 0, cajas_totales);
}

void* control(void* arg){
	while(!fin){
		sem_wait(&semaphore);

		pthread_mutex_lock(&mutex);
		cajas_ocupadas++;
		pthread_mutex_unlock(&mutex);

		sleep(2);

		pthread_mutex_lock(&mutex);
		cajas_ocupadas--;
		pthread_mutex_unlock(&mutex);

		sem_post(&semaphore);

		sleep(1);
	}
}


int main (){

	pthread_t clientes[SIZEROW]; /*50 clientes*/
	init(NCAJAS);
	int contador = 50;
	for(int i=0; i<SIZEROW; i++)
		pthread_create(&clientes[i], NULL, &control, NULL);

	while(contador){
		contador--;
		printf("Hay %i cajas usadas   \r",cajas_ocupadas);
		fflush(stdout);
		sleep(1);
	}

	for(int i=0; i<NCAJAS; i++)
		pthread_join(clientes[i], NULL);

	sem_destroy(&semaphore);
	pthread_mutex_destroy(&mutex);


	return EXIT_SUCCESS;

}
