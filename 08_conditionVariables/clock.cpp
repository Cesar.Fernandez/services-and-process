#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

int thread_flag;
int contador;
pthread_cond_t thread_flag_cv;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void initialize_flag(){
	pthread_cond_init (&thread_flag_cv, NULL);
	thread_flag = 0;
}

void* clock(void * arg){
	int s = 0;
	printf("Han pasado %i segundos\r", s);
	while(1){
		fflush(stdout);
		s += 5;
		printf("Han pasado %i segundos\r", s);
		sleep(5);
	}

}

int main (){
	pthread_t thread;

	pthread_create(&thread, NULL, &clock, NULL);

	pthread_join(thread, NULL);


	return EXIT_SUCCESS;

}
