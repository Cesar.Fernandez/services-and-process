#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

typedef int pid_t;

pid_t  spawn(char* comando, char *lc []){
	pid_t child_pid;

	child_pid = fork();

	if(child_pid != 0){
		/* Soy el padre*/
	}else{
		execvp(comando, lc);
		fprintf(stderr, "an error occurred in execvp\n");
		abort();
	}
	return child_pid;
}


int main(int argc, char* argv[]){
	pid_t child_pid;
	int child_status;
	int resultado;

	child_pid = spawn ("./suma", argv);
	wait (&child_status);

	if(WIFEXITED(child_status)) {
		resultado = WEXITSTATUS(child_status);
		printf("La suma vale %i\n", resultado);
	}


	return EXIT_SUCCESS;
}
