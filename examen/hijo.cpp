#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>

#define N 3

sig_atomic_t sigusr1_count;

void handler(int signal_number){
	++sigusr1_count;
}

int main (){

	sigusr1_count = 0;
	int i;
	struct sigaction sa;

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &handler;
	sigaction(SIGUSR1, &sa, NULL);

	while(sigusr1_count < N){
		sleep(2);
		printf("Me has llamado %i veces\n", sigusr1_count);
	}

	printf("Exito\n");


	return EXIT_SUCCESS;

}
