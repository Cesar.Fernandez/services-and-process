#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#include <stdlib.h>
#include <stdio.h>
#ifdef __cplusplus
extern "C" {
#endif
int sumar(int n1, int n2);
int restar(int n1, int n2);

#ifdef __cplusplus
}
#endif
#endif
