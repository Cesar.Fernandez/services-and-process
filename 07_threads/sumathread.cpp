#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

void* sumaFunc(void * arg){
	int *n = (int *) arg;
	int c = 2;
	sleep(1);
	printf("Wait! I`m Doing the sum....\n");
	sleep(2);
	printf("I`m done\n");
	sleep(1);
	pthread_mutex_lock(&mutex1);
	(*n) += c;
	pthread_mutex_unlock(&mutex1);

	return NULL;
}

int main (){
	pthread_t newthread;
	int n = 5;
	int p = n;

	pthread_create(&newthread, NULL, sumaFunc, &n);

	pthread_join(newthread, NULL);

	printf("Here the result of 2 + %i = %i\n", p, n);


	return EXIT_SUCCESS;

}
