#include "lib2.h"

int multiplicar(int op1, int op2){
	return op1 * op2;
}

int factorial(int op1){
	if(op1 == 1)
		return 1;
	return op1 * factorial(op1-1);
}

void catalogo(){
	printf("Las funciones de la lib2 son MULTIPLICAR y FACTORIAL\n"); 
}
