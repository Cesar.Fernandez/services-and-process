#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t sigusr1_count=0;

void handler (int signal_number){
	++sigusr1_count;
}

int main () {
	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &handler;
	//sigaction (SIGINT,&sa, NULL);
	signal(SIGINT, handler);

	printf("PID = %i\n", (int) getpid());
	sleep(10);

	signal(SIGINT, SIG_DFL);


	printf("SIGINT was raised %d times\n", sigusr1_count);
	return EXIT_SUCCESS;
}
