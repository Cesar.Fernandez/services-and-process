#ifndef __LIB1_H__
#define __LIB1_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif
int sumar(int op1, int op2);
int restar(int op1, int op2);

char catalgo ();
#ifdef __cplusplus
}
#endif
#endif
