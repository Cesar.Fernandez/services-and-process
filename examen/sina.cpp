#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define M 15
#define N 2


sem_t semaphore;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int ladrillosF;
int ladrillosDejados;

void* miFabrica(void* arg){
    while(ladrillosF<M){
        sem_post(&semaphore);
        ladrillosF++;
        printf("Ladrillos fabricados : %i\n", ladrillosF);
        sleep(1);
    }
    return NULL;
}

void* recoger(void* arg){
    int random;
    random = rand() %2 + 1;
    while(ladrillosDejados<M){
        pthread_mutex_lock(&mutex);
        if(random+ladrillosDejados>20)
            random=1;

        if(random+ladrillosDejados <= 20){
            if(random+ladrillosDejados <= ladrillosF){
                for(int i=0; i<random; i++){
                    sem_wait(&semaphore);
                }

                printf("Cojo %i\n", random);

                for(int i=0; i<random; i++){
                    ladrillosDejados++;
                }
            }else{
              sleep(1);
              printf("Esperando a la fabrica\n");
            }
        }
        printf("Recogidos %i\n", ladrillosDejados);
        pthread_mutex_unlock(&mutex);
        sleep(2);
    }
    printf("Terminamos\n");
    return NULL;
}

int main (){

    ladrillosF = 0;
    ladrillosDejados = 0;
    pthread_t thread[N];
    pthread_t fab;

    sem_init(&semaphore, 0,0);

    pthread_create(&fab, NULL, &miFabrica, NULL);

    for(int i=0; i<N; i++){
        pthread_create(&thread[i], NULL, &recoger, NULL);
    }




    pthread_join(fab,NULL);

    for(int i=0; i<N; i++){
        pthread_join(thread[i], NULL);
    }
    sem_destroy(&semaphore);
    pthread_mutex_destroy(&mutex);


    return EXIT_SUCCESS;

}
